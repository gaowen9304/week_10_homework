import requests
import time
import base64
from requests.exceptions import RequestException
from lxml import etree
from bs4 import BeautifulSoup
from pyquery import PyQuery
import json
import re


def getPage(url):
    '''爬取指定url地址的信息'''
    try:
        # 定义请求头信息
        headers = {
            'User-Agent': 'User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36'}
        # 执行爬取
        res = requests.get(url, headers=headers)

        # 判断并返回结果
        if res.status_code == 200:
            return res.text
        else:
            return None
    except RequestException:
        return None


def parsePage(content, keyword, offset):
    '''解析爬取网页中的内容，并返回字段结果'''
    index = offset * 25 + 1
    if keyword == 2:
        # ===============使用BeautifulSoup=======================
        # 初始化，返回使用BeautifulSoup对象
        html = BeautifulSoup(content, 'lxml')
        # 解析网页中<tr class='item'>...</tr>标签信息（每本书的信息）
        items = html.select("tr.item")
        # 遍历并解析每本书的具体信息
        for item in items:
            yield {
                'index': int(re.findall(".*?'(.*?)'.*?", item.select("td a.nbg")[0].attrs['onclick'])[0]) + index,
                'name': item.select("td div.pl2 a")[0].attrs['title'].strip(),
                'image': item.select("td a.nbg img")[0].attrs['src'],
                'actor': item.select("td p.pl")[0].get_text().strip(),
                'score': item.select("td div span.rating_nums")[0].get_text(),
            }
    elif keyword == 3:
        # ===============使用PyQuery=======================
        # 初始化，返回PyQuery对象
        doc = PyQuery(content)
        # 解析网页中<tr class='item'>...</tr>标签信息（每本书的信息）
        items = doc("tr.item")
        # 遍历并解析每本书的具体信息

        for item in items.items():
            yield {
                'index': int(re.findall(".*?'(.*?)'.*?", item.find("td a.nbg").attr('onclick'))[0]) + index,
                'name': item.find('td div.pl2 a').text(),
                'image': item.find('td a img').attr('src'),
                'actor': item.find('td p.pl').text(),
                'score': item.find('td div span.rating_nums').text(),
            }
    else:
        # ===============使用xpath=======================
        # 初始化，返回根节点对象
        html = etree.HTML(content)
        # 解析网页中<tr class='item'>...</tr>标签信息（每本书的信息）
        items = html.xpath("//tr[@class='item']")
        # 遍历并解析每本书的具体信息
        for item in items:
            yield {
                'index': int(re.findall(".*?'(.*?)'.*?", item.xpath(".//td[@width='100']/a/@onclick")[0])[0]) + index,
                'name': item.xpath(".//div[@class='pl2']/a/text()")[0].strip(),
                'image': item.xpath(".//td[@width='100']/a/img/@src")[0],
                'actor': item.xpath(".//p[@class='pl']/text()")[0].strip(),
                'score': item.xpath(".//div[@class='star clearfix']/span[@class='rating_nums']/text()")[0].strip(),
            }


def writeFile(content):
    '''执行文件追加写操作'''
    with open("./result.txt", 'a', encoding='utf-8') as f:
        f.write(json.dumps(content, ensure_ascii=False) + '\n')


def main(keyword, offset):
    '''主程序函数，负责调度执行爬虫处理'''
    url = "https://book.douban.com/top250?start=" + str(offset*25)
    print(url)
    html = getPage(url)  # 执行爬取
    # print(html)
    if html:
        for item in parsePage(html, keyword, offset):  # 执行解析并遍历
            print(item)
            writeFile(item)  # 执行写操作


if __name__ == '__main__':
    # main(0)
    keyword = int(input('请选择解析方式（xpath(默认):1 ,BeautifulSoup:2 ,PyQuery:3）：'))
    for i in range(10):
        print("=" * 20, '第%d页' % (i + 1), "=" * 20)
        main(keyword, offset=i)
        time.sleep(1)
    print("=" * 60)
    print('爬取结束')
